package com.viettel.bss.viettelpos.v4.plan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.plan.model.InfrastureModel;
import com.viettel.bss.viettelpos.v4.utils.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by root on 05/01/2018.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<String> headers;
    private HashMap<String, ArrayList<InfrastureModel>> dataChild;
    private ChooseItemListener chooseItemListener;

    public ExpandableListAdapter(Context context, ArrayList<String> headers,
                                 HashMap<String, ArrayList<InfrastureModel>> dataChild,
                                 ChooseItemListener chooseItemListener) {
        this.context = context;
        this.headers = headers;
        this.dataChild = dataChild;
        this.chooseItemListener = chooseItemListener;
    }



    @Override
    public int getGroupCount() {
        return (headers == null) ? 0 : headers.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return (this.dataChild.get(this.headers.get(groupPosition)) == null ) ? 0
                : this.dataChild.get(this.headers.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.headers.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.dataChild.get(this.headers.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = headers.get(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.choose_infrastructure_header_item, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.title_tv);
        lblListHeader.setText(headerTitle);
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                chooseItemListener.onGroupClick(v, groupPosition);
//            }
//        });

//        if (isExpanded)
//            convertView.setPadding(0, 0, 0, 0);
//        else
//            convertView.setPadding(0, 0, 0, 20);

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final InfrastureModel infrastureModel = dataChild.get(headers.get(groupPosition)).get(childPosition);
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater)
                    this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.choose_infrastructure_item_item, null);
        }
        TextView titleTv = (TextView) convertView.findViewById(R.id.title_tv);
        TextView codeTv = (TextView) convertView.findViewById(R.id.code_tv);
        TextView addressTv = (TextView) convertView.findViewById(R.id.address_tv);
        final RadioButton radio = (RadioButton) convertView.findViewById(R.id.radio);
        final TextView descTv = (TextView) convertView.findViewById(R.id.desc_tv);
        final View detailView = convertView.findViewById(R.id.detail_view);

        if(!CommonActivity.isNullOrEmpty(infrastureModel.getObjectName())){
            titleTv.setText(infrastureModel.getObjectName());
            titleTv.setVisibility(View.VISIBLE);
        } else {
            titleTv.setVisibility(View.GONE);
        }

        if(!CommonActivity.isNullOrEmpty(infrastureModel.getObjectCode())){
            codeTv.setText(infrastureModel.getObjectCode());
            codeTv.setVisibility(View.VISIBLE);
        } else {
            codeTv.setVisibility(View.GONE);
        }

        if(!CommonActivity.isNullOrEmpty(infrastureModel.getAddress())){
            addressTv.setText(infrastureModel.getAddress());
            addressTv.setVisibility(View.VISIBLE);
        } else {
            addressTv.setVisibility(View.GONE);
        }

        if(infrastureModel.getDescription() != null){
            descTv.setText(infrastureModel.getDescription());
        }
        if(infrastureModel.isChecked()){
            radio.setChecked(true);
        } else {
            radio.setChecked(false);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseItemListener.onChoose(v, groupPosition, childPosition, true);
            }
        });

        radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseItemListener.onChoose(v, groupPosition, childPosition, true);
            }
        });

        descTv.setVisibility(infrastureModel.isViewDetail() ? View.VISIBLE : View.GONE);

        if(CommonActivity.isNullOrEmpty(infrastureModel.getDescription())){
            detailView.setVisibility(View.GONE);
            descTv.setVisibility(View.GONE);
        } else {
            detailView.setVisibility(View.VISIBLE);
        }
        detailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infrastureModel.setViewDetail(!infrastureModel.isViewDetail());
                if(descTv.getVisibility() == View.GONE){
                    descTv.setVisibility(View.VISIBLE);
                } else {
                    descTv.setVisibility(View.GONE);
                }
            }
        });
        descTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descTv.setVisibility(View.GONE);
            }
        });

        titleTv.setVisibility(View.VISIBLE);
        Long objectType = infrastureModel.getObjectType();
        if(null != objectType) {
            if ("2".equals(objectType + "")) {
                titleTv.setVisibility(View.VISIBLE);
            } else {
                titleTv.setVisibility(View.GONE);
            }
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public interface ChooseItemListener {
        void onChoose(View view, int groupPosition, int chilPosition, boolean checked);

        void onGroupClick(View view, int groupPosition);
    }
}
