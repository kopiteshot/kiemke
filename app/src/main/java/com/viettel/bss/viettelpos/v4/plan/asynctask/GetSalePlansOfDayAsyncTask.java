package com.viettel.bss.viettelpos.v4.plan.asynctask;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.asyntask.AsyncTaskCommonSupper;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.plan.model.SalePlansOfDayModel;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

/**
 * Created by BaVV on 1/05/2018.
 */

public class GetSalePlansOfDayAsyncTask extends AsyncTaskCommonSupper<String, Void, SalePlansOfDayModel> {

    public GetSalePlansOfDayAsyncTask(
            Activity context,
            OnPostExecuteListener<SalePlansOfDayModel> listener,
            View.OnClickListener moveLogInAct) {

        super(context, listener, moveLogInAct);
    }

    @Override
    protected SalePlansOfDayModel doInBackground(String... params) {
        return getSalePlansOfDay(params[0], params[1], Integer.parseInt(params[2]), Integer.parseInt(params[3]));
    }

//    isdn: h004_ftth_thaott6 idNo: 1232342343 token: f3b53334b42b43bdbe5dd74420b09ca3
    private SalePlansOfDayModel getSalePlansOfDay(String fromDate, String toDate, int startRow, int pageLength) {

        String original = "";
        SalePlansOfDayModel salePlansOfDayModel = null;
        String func = "getSalePlansOfDay";

        try {
            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_" + func);
            StringBuilder rawData = new StringBuilder();

            rawData.append("<ws:" + func + ">");
            rawData.append("<input>");
            rawData.append("<token>" + Session.getToken() + "</token>");

            if (!CommonActivity.isNullOrEmpty(fromDate))
                rawData.append("<fromDate>").append(fromDate).append("</fromDate>");

            if (!CommonActivity.isNullOrEmpty(toDate))
                rawData.append("<toDate>").append(toDate).append("</toDate>");

            if (!CommonActivity.isNullOrEmpty(startRow))
                rawData.append("<startRow>").append(startRow).append("</startRow>");

            if (!CommonActivity.isNullOrEmpty(pageLength))
                rawData.append("<pageLength>").append(pageLength).append("</pageLength>");

            rawData.append("</input>");
            rawData.append("</ws:" + func + ">");

            Log.i("RowData", rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData.toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope, Constant.BCCS_GW_URL,
                    mActivity, "mbccs_" + func);
            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i("Original", response);

            Serializer serializer = new Persister();
            salePlansOfDayModel = serializer.read(SalePlansOfDayModel.class, original);
        } catch (Exception e) {
            Log.e(Constant.TAG, "blockSubForTerminate", e);
            description = e.getMessage();
            errorCode = Constant.ERROR_CODE;
        }

        if (CommonActivity.isNullOrEmpty(salePlansOfDayModel)) {
            description = mActivity.getString(R.string.no_return_from_system);
            errorCode = Constant.ERROR_CODE;
            return null;
        } else {
//            description = subscriberModel.getDescription();
            errorCode = salePlansOfDayModel.getErrorCode();

            if (description != null && description.contains("java.lang.String.length()")) {
                description = mActivity.getString(R.string.server_time_out);
            }
        }
        return salePlansOfDayModel;
    }
}