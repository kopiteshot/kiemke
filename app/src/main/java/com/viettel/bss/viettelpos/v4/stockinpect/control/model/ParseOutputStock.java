package com.viettel.bss.viettelpos.v4.stockinpect.control.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

@Root(name = "return", strict = false)
public class ParseOutputStock implements Serializable {
    @Element(name = "errorCode", required = false)
    private String errorCode;
    @Element(name = "description", required = false)
    private String description;

    @Element(name = "")
    public String getErrorCode() {
        return errorCode;
    }

    public String getDescription() {
        return description;
    }
}
