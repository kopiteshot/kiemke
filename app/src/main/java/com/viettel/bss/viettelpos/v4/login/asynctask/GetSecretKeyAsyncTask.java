package com.viettel.bss.viettelpos.v4.login.asynctask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.synchronizationdata.XmlDomParse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Created by hungtv64 on 1/26/2018.
 */

public class GetSecretKeyAsyncTask extends AsyncTask<String, Void, String> {

    private final ProgressDialog progress;
    private final XmlDomParse parse = new XmlDomParse();
    private final OnPostExecuteListener<String> listener;

    private Activity activity;
    private String errorCode = "";
    private String description = "";

    public GetSecretKeyAsyncTask(Activity activity, OnPostExecuteListener<String> listener) {

        this.activity = activity;
        this.listener = listener;

        this.progress = new ProgressDialog(activity);
        // check font
        this.progress.setCancelable(false);
        this.progress.setMessage(activity.getResources().getString(R.string.waitting));
        if (!this.progress.isShowing()) {
            this.progress.show();
        }
    }

    @Override
    protected String doInBackground(String... arg0) {
        if (!CommonActivity.isInternetReachable()) {
            return Constant.ERROR_PING_SERVER;
        }
        return getSecretKey(arg0[0]);
    }

    @Override
    protected void onPostExecute(String result) {
        if (!activity.isFinishing() && progress != null && progress.isShowing()) {
            progress.dismiss();
        }
        listener.onPostExecute(result, errorCode, description);
    }

    private String getSecretKey(String userName) {
        String original = "";
        try {
            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_getSecretKey");
            StringBuilder rawData = new StringBuilder();
            rawData.append("<ws:getSecretKey>");
            rawData.append("<input>");
            rawData.append("<userName>").append(userName);
            rawData.append("</userName>");
            rawData.append("</input>");
            rawData.append("</ws:getSecretKey>");
            Log.i("RowData", rawData.toString());

            String envelope = input.buildInputGatewayWithRawData(rawData.toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope,
                    Constant.BCCS_GW_URL, activity, "mbccs_getSecretKey");
            Log.i("Responseeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i("original123", original);

            // ==== parse xml list ip
            Document doc = parse.getDomElement(original);
            NodeList nl = doc.getElementsByTagName("return");
            for (int i = 0; i < nl.getLength(); i++) {
                Element e2 = (Element) nl.item(i);
                errorCode = parse.getValue(e2, "errorCode");
                Log.d("errorCode", errorCode);
                description = parse.getValue(e2, "description");
                Log.d("description", description);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return errorCode;
    }
}