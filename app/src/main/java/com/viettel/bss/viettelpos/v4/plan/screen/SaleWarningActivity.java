package com.viettel.bss.viettelpos.v4.plan.screen;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.dialog.LoginDialog;
import com.viettel.bss.viettelpos.v4.plan.asynctask.GetSalePlansResultAsyncTask;
import com.viettel.bss.viettelpos.v4.plan.asynctask.GetThreeDayAlarmKpiPTMAsyncTask;
import com.viettel.bss.viettelpos.v4.plan.event.PlanEvent;
import com.viettel.bss.viettelpos.v4.plan.model.AlarmKpiCurrentModel;
import com.viettel.bss.viettelpos.v4.plan.model.SalePlansResultModel;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SaleWarningActivity extends AppCompatActivity {
    @BindView(R.id.pager)
    ViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private String planDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_warning);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Kết quả cảnh báo bán hàng");

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("CẢNH BÁO BÁN HÀNG"));
        tabLayout.addTab(tabLayout.newTab().setText("KẾT QUẢ BÁN HÀNG"));
        tabLayout.addTab(tabLayout.newTab().setText("XỬ LÝ CẢNH BÁO"));
//        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.setOffscreenPageLimit(3);

        initData();
    }

    private void initData() {
        Bundle b = getIntent().getExtras();
        if(null != b) {
            planDate = b.getString("planDate");
        }

        getSalePlansResult();
        getSaleAlarmKpi();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 1:
                    TabSaleWarningFragment saleWarningFragment = new TabSaleWarningFragment();
                    return saleWarningFragment;
                case 2:
                    TabSaleResultFragment saleResultFragment = new TabSaleResultFragment();
                    return saleResultFragment;
                case 0:
                    TabSaleAlarmKpiFragment saleAlarmKpiFragment = new TabSaleAlarmKpiFragment();
                    return saleAlarmKpiFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    private void getSalePlansResult() {

        if (!CommonActivity.isNetworkConnected(getApplicationContext())) {
            CommonActivity.createAlertDialog(this,
                    R.string.errorNetwork, R.string.app_name).show();
            return;
        }

        new GetSalePlansResultAsyncTask(this, new OnPostExecuteListener<SalePlansResultModel>() {
            @Override
            public void onPostExecute(SalePlansResultModel result, String errorCode, String description) {
                if ("0".equals(errorCode)) {
                    if (null != result) {
                        EventBus.getDefault().post(new PlanEvent(PlanEvent.Action.RELOAD_SALE_RESULT, result));
                        EventBus.getDefault().post(new PlanEvent(PlanEvent.Action.RELOAD_SALE_WARNING, result));
                        Log.e("@@@@", result.toString());
                    }
                } else if ("TOKEN_INVALID".equals(errorCode)) {
                    Dialog dialog = CommonActivity
                            .createAlertDialog(SaleWarningActivity.this,
                                    R.string.token_invalid, R.string.app_name,
                                    moveLogInAct);
                    dialog.show();
                } else {
                    //todo - no data
                }
            }
        }, moveLogInAct).execute(planDate);
    }

    private void getSaleAlarmKpi() {

        if (!CommonActivity.isNetworkConnected(getApplicationContext())) {
            CommonActivity.createAlertDialog(this,
                    R.string.errorNetwork, R.string.app_name).show();
            return;
        }

        new GetThreeDayAlarmKpiPTMAsyncTask(this, new OnPostExecuteListener<AlarmKpiCurrentModel>() {
            @Override
            public void onPostExecute(AlarmKpiCurrentModel result, String errorCode, String description) {
                if ("0".equals(errorCode)) {
                    if (null != result) {
                        EventBus.getDefault().post(new PlanEvent(PlanEvent.Action.RELOAD_SALE_ALARM_KPI, result));
                        Log.e("@@@@", result.toString());
                    }
                } else if ("TOKEN_INVALID".equals(errorCode)) {
                    Dialog dialog = CommonActivity
                            .createAlertDialog(SaleWarningActivity.this,
                                    R.string.token_invalid, R.string.app_name,
                                    moveLogInAct);
                    dialog.show();
                } else {
                    //todo - no data
                }
            }
        }, moveLogInAct).execute();
    }

    protected final View.OnClickListener moveLogInAct = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LoginDialog dialog = new LoginDialog(SaleWarningActivity.this, "");
            dialog.show();
        }
    };
}
