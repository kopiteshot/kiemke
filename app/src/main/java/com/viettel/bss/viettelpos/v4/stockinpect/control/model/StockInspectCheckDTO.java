package com.viettel.bss.viettelpos.v4.stockinpect.control.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

@Root(name = "stockInspectCheckDTO", strict = false)
public class StockInspectCheckDTO extends BaseDTO implements Serializable {
    @Element(name = "approveNote", required = false)
    protected String approveNote;
    @Element(name = "availableQuantity", required = false)
    protected Long availableQuantity;
    @Element(name = "currentQuantity", required = false)
    protected Long currentQuantity;
    @Element(name = "disableQuantityPoor", required = false)
    protected boolean disableQuantityPoor;
    @Element(name = "fromSerial", required = false)
    protected String fromSerial;
    @Element(name = "impType", required = false)
    protected Long impType;
    @Element(name = "inspectStatus", required = false)
    protected Long inspectStatus;
    @Element(name = "note", required = false)
    protected String note;
    @Element(name = "ownerId", required = false)
    protected Long ownerId;
    @Element(name = "ownerType", required = false)
    protected Long ownerType;
    @Element(name = "prodOfferId", required = false)
    protected Long prodOfferId;
    @Element(name = "productCode", required = false)
    protected String productCode;
    @Element(name = "productName", required = false)
    protected String productName;
    @Element(name = "productOfferTypeId", required = false)
    protected Long productOfferTypeId;
    @Element(name = "quanittyReal", required = false)
    protected Long quanittyReal;
    @Element(name = "quantity", required = false)
    protected Long quantity;
    @Element(name = "quantityPoor", required = false)
    protected Long quantityPoor;
    @Element(name = "quantityStockCheck", required = false)
    protected Long quantityStockCheck;
    @Element(name = "reason", required = false)
    protected String reason;
    @Element(name = "saleOrExportDate", required = false)
    protected XMLGregorianCalendar saleOrExportDate;
    @Element(name = "stateId", required = false)
    protected Long stateId;
    @Element(name = "stateName", required = false)
    protected String stateName;
    @Element(name = "stockInspectRealId", required = false)
    protected Long stockInspectRealId;
    @Element(name = "toSerial", required = false)
    protected String toSerial;

    public StockInspectCheckDTO(String approveNote, Long availableQuantity, Long currentQuantity, boolean disableQuantityPoor, String fromSerial, Long impType, Long inspectStatus, String note, Long ownerId, Long ownerType, Long prodOfferId, String productCode, String productName, Long productOfferTypeId, Long quanittyReal, Long quantity, Long quantityPoor, Long quantityStockCheck, String reason, XMLGregorianCalendar saleOrExportDate, Long stateId, String stateName, Long stockInspectRealId, String toSerial) {
        this.approveNote = approveNote;
        this.availableQuantity = availableQuantity;
        this.currentQuantity = currentQuantity;
        this.disableQuantityPoor = disableQuantityPoor;
        this.fromSerial = fromSerial;
        this.impType = impType;
        this.inspectStatus = inspectStatus;
        this.note = note;
        this.ownerId = ownerId;
        this.ownerType = ownerType;
        this.prodOfferId = prodOfferId;
        this.productCode = productCode;
        this.productName = productName;
        this.productOfferTypeId = productOfferTypeId;
        this.quanittyReal = quanittyReal;
        this.quantity = quantity;
        this.quantityPoor = quantityPoor;
        this.quantityStockCheck = quantityStockCheck;
        this.reason = reason;
        this.saleOrExportDate = saleOrExportDate;
        this.stateId = stateId;
        this.stateName = stateName;
        this.stockInspectRealId = stockInspectRealId;
        this.toSerial = toSerial;
    }

    public String getApproveNote() {
        return approveNote;
    }

    public Long getAvailableQuantity() {
        return availableQuantity;
    }

    public Long getCurrentQuantity() {
        return currentQuantity;
    }

    public boolean isDisableQuantityPoor() {
        return disableQuantityPoor;
    }

    public String getFromSerial() {
        return fromSerial;
    }

    public Long getImpType() {
        return impType;
    }

    public Long getInspectStatus() {
        return inspectStatus;
    }

    public String getNote() {
        return note;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public Long getOwnerType() {
        return ownerType;
    }

    public Long getProdOfferId() {
        return prodOfferId;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getProductName() {
        return productName;
    }

    public Long getProductOfferTypeId() {
        return productOfferTypeId;
    }

    public Long getQuanittyReal() {
        return quanittyReal;
    }

    public Long getQuantity() {
        return quantity;
    }

    public Long getQuantityPoor() {
        return quantityPoor;
    }

    public Long getQuantityStockCheck() {
        return quantityStockCheck;
    }

    public String getReason() {
        return reason;
    }

    public XMLGregorianCalendar getSaleOrExportDate() {
        return saleOrExportDate;
    }

    public Long getStateId() {
        return stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public Long getStockInspectRealId() {
        return stockInspectRealId;
    }

    public String getToSerial() {
        return toSerial;
    }
}
