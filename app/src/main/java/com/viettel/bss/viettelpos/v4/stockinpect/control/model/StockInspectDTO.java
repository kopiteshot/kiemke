package com.viettel.bss.viettelpos.v4.stockinpect.control.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.io.Serializable;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;


public class StockInspectDTO extends BaseDTO implements Serializable {
    @Element(name = "approveDate", required = false)
    protected XMLGregorianCalendar approveDate;
    @Element(name = "approveNote", required = false)
    protected String approveNote;
    @Element(name = "approveStatus", required = false)
    protected Long approveStatus;
    @Element(name = "approveUser", required = false)
    protected String approveUser;
    @Element(name = "approveUserId", required = false)
    protected Long approveUserId;
    @Element(name = "approveUserName", required = false)
    protected String approveUserName;
    @Element(name = "createDate", required = false)
    protected XMLGregorianCalendar createDate;
    @Element(name = "createUser", required = false)
    protected String createUser;
    @Element(name = "createUserId", required = false)
    protected Long createUserId;
    @Element(name = "createUserName", required = false)
    protected String createUserName;
    @Element(name = "inspectStatus", required = false)
    protected Long inspectStatus;
    @Element(name = "isFinished", required = false)
    protected Long isFinished;
    @Element(name = "isFinishedAll", required = false)
    protected Long isFinishedAll;
    @Element(name = "isScan", required = false)
    protected Long isScan;
    @Element(name = "note", required = false)
    protected String note;
    @Element(name = "ownerId", required = false)
    protected Long ownerId;
    @Element(name = "ownerType", required = false)
    protected Long ownerType;
    @Element(name = "prodOfferId", required = false)
    protected Long prodOfferId;
    @Element(name = "prodOfferTypeId", required = false)
    protected Long prodOfferTypeId;
    @Element(name = "quantityFinance", required = false)
    protected Long quantityFinance;
    @Element(name = "quantityPoor", required = false)
    protected Long quantityPoor;
    @Element(name = "quantityReal", required = false)
    protected Long quantityReal;
    @Element(name = "quantitySys", required = false)
    protected Long quantitySys;
    @Element(name = "shopCode", required = false)
    protected String shopCode;
    @Element(name = "shopId", required = false)
    protected Long shopId;
    @Element(name = "shopName", required = false)
    protected String shopName;
    @Element(name = "stateId", required = false)
    protected Long stateId;
    @Element(name = "stockInspectId", required = false)
    protected Long stockInspectId;
    @ElementList(name = "stockInspectRealDTOs", entry = "stockInspectRealDTOs", required = false)
    protected List<StockInspectRealDTO> stockInspectRealDTOs;
    @Element(name = "updateDate", required = false)
    protected XMLGregorianCalendar updateDate;

    public XMLGregorianCalendar getApproveDate() {
        return approveDate;
    }

    public String getApproveNote() {
        return approveNote;
    }

    public Long getApproveStatus() {
        return approveStatus;
    }

    public String getApproveUser() {
        return approveUser;
    }

    public Long getApproveUserId() {
        return approveUserId;
    }

    public String getApproveUserName() {
        return approveUserName;
    }

    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public Long getInspectStatus() {
        return inspectStatus;
    }

    public Long getIsFinished() {
        return isFinished;
    }

    public Long getIsFinishedAll() {
        return isFinishedAll;
    }

    public Long getIsScan() {
        return isScan;
    }

    public String getNote() {
        return note;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public Long getOwnerType() {
        return ownerType;
    }

    public Long getProdOfferId() {
        return prodOfferId;
    }

    public Long getProdOfferTypeId() {
        return prodOfferTypeId;
    }

    public Long getQuantityFinance() {
        return quantityFinance;
    }

    public Long getQuantityPoor() {
        return quantityPoor;
    }

    public Long getQuantityReal() {
        return quantityReal;
    }

    public Long getQuantitySys() {
        return quantitySys;
    }

    public String getShopCode() {
        return shopCode;
    }

    public Long getShopId() {
        return shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public Long getStateId() {
        return stateId;
    }

    public Long getStockInspectId() {
        return stockInspectId;
    }

    public List<StockInspectRealDTO> getStockInspectRealDTOs() {
        return stockInspectRealDTOs;
    }

    public XMLGregorianCalendar getUpdateDate() {
        return updateDate;
    }

    public void setApproveDate(XMLGregorianCalendar approveDate) {
        this.approveDate = approveDate;
    }

    public void setApproveNote(String approveNote) {
        this.approveNote = approveNote;
    }

    public void setApproveStatus(Long approveStatus) {
        this.approveStatus = approveStatus;
    }

    public void setApproveUser(String approveUser) {
        this.approveUser = approveUser;
    }

    public void setApproveUserId(Long approveUserId) {
        this.approveUserId = approveUserId;
    }

    public void setApproveUserName(String approveUserName) {
        this.approveUserName = approveUserName;
    }

    public void setCreateDate(XMLGregorianCalendar createDate) {
        this.createDate = createDate;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public void setInspectStatus(Long inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    public void setIsFinished(Long isFinished) {
        this.isFinished = isFinished;
    }

    public void setIsFinishedAll(Long isFinishedAll) {
        this.isFinishedAll = isFinishedAll;
    }

    public void setIsScan(Long isScan) {
        this.isScan = isScan;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public void setOwnerType(Long ownerType) {
        this.ownerType = ownerType;
    }

    public void setProdOfferId(Long prodOfferId) {
        this.prodOfferId = prodOfferId;
    }

    public void setProdOfferTypeId(Long prodOfferTypeId) {
        this.prodOfferTypeId = prodOfferTypeId;
    }

    public void setQuantityFinance(Long quantityFinance) {
        this.quantityFinance = quantityFinance;
    }

    public void setQuantityPoor(Long quantityPoor) {
        this.quantityPoor = quantityPoor;
    }

    public void setQuantityReal(Long quantityReal) {
        this.quantityReal = quantityReal;
    }

    public void setQuantitySys(Long quantitySys) {
        this.quantitySys = quantitySys;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public void setStateId(Long stateId) {
        this.stateId = stateId;
    }

    public void setStockInspectId(Long stockInspectId) {
        this.stockInspectId = stockInspectId;
    }

    public void setStockInspectRealDTOs(List<StockInspectRealDTO> stockInspectRealDTOs) {
        this.stockInspectRealDTOs = stockInspectRealDTOs;
    }

    public void setUpdateDate(XMLGregorianCalendar updateDate) {
        this.updateDate = updateDate;
    }
}
