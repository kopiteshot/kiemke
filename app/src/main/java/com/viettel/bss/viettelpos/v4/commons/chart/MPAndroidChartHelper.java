package com.viettel.bss.viettelpos.v4.commons.chart;

import android.content.Context;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.report.object.ItemBean;

import java.util.ArrayList;
import java.util.List;

import static com.github.mikephil.charting.utils.ColorTemplate.rgb;


/**
 * Created by Toancx on 1/5/2017.
 */

public class MPAndroidChartHelper {
    private static final String TAG = "MPAndroidChartHelper";
    private static final int[] MATERIAL_COLORS = {
            rgb("#2ecc71"), rgb("#f1c40f"), rgb("#e74c3c"), rgb("#3498db"), rgb("#FFFF7043"), rgb("#FFFFAB40"), rgb("#FFFBC02D")
            , rgb("#FFEEFF41"), rgb("#FFB2FF59"), rgb("#FF64DD17"), rgb("#FF00E676"), rgb("#FF1DE9B6"), rgb("#FF00E5FF"), rgb("#FF00B0FF")
            , rgb("#FFAA00FF"), rgb("#FF2979FF"), rgb("#FF651FFF"), rgb("#FF304FFE"), rgb("#FFD500F9"), rgb("#FFF50057")
    };

    public static void drawBarChartGroup(BarChart barChart, List<ItemBean> lstItemBean, Context context) {
        barChart.getDescription().setEnabled(true);
        barChart.setPinchZoom(true);
        barChart.setDrawBarShadow(true);
        barChart.setDrawGridBackground(true);

        MyMarkerView mv = new MyMarkerView(context, R.layout.custom_marker_view);
        mv.setChartView(barChart);
        barChart.setMarker(mv);

        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(true);
//        l.setTypeface(Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf"));
        l.setYOffset(0f);
        l.setXOffset(10f);
        l.setYEntrySpace(0f);
        l.setTextSize(8f);

        XAxis xAxis = barChart.getXAxis();
//        xAxis.setTypeface(Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf"));
        xAxis.setGranularity(1f);
        xAxis.setCenterAxisLabels(true);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return String.valueOf((int) value);
            }
        });

        YAxis leftAxis = barChart.getAxisLeft();
//        leftAxis.setTypeface(Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf"));
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(35f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        barChart.getAxisRight().setEnabled(false);

        List<ItemBean> lstData = new ArrayList<>();
        ItemBean currentItemBean = null;
        for (ItemBean itemBean : lstItemBean) {
            if (currentItemBean == null || !currentItemBean.getGroupName().equals(itemBean.getGroupName())) {
                currentItemBean = itemBean;
                lstData.add(currentItemBean);
            }

            currentItemBean.getLstItemBean().add(itemBean);
        }

        float groupSpace = 0.08f;
        float barSpace = 0.03f; // x4 DataSet
        float barWidth = 0.2f; // x4 DataSet
        int start = 1;
        int groupCount = lstData.size();

        List<ArrayList<BarEntry>> lstBarEntry = new ArrayList<>();
        for (int i = 0; i < currentItemBean.getLstItemBean().size(); i++) {
            lstBarEntry.add(new ArrayList<BarEntry>());
        }

        for (int i = 0; i < lstData.size(); i++) {
            ItemBean itemBean = lstData.get(i);
            for (int j = 0; j < itemBean.getLstItemBean().size(); j++) {
                lstBarEntry.get(j).add(new BarEntry(i, itemBean.getLstItemBean().get(j).getValue()));
            }
        }

        if(barChart.getData() != null && barChart.getData().getDataSetCount() > 0){
            for(int i =0; i< barChart.getData().getDataSetCount(); i++){
                ((BarDataSet)barChart.getData().getDataSetByIndex(i)).setValues(lstBarEntry.get(i));
            }
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {
            List<BarDataSet> lstBarDataSet = new ArrayList<>();
            int index = 0;
            for(int i = 0; i< currentItemBean.getLstItemBean().size(); i++){
                BarDataSet barDataSet = new BarDataSet(lstBarEntry.get(i), currentItemBean.getLstItemBean().get(i).getName());
                if(index < MATERIAL_COLORS.length) {
                    barDataSet.setColors(MATERIAL_COLORS[index++]);
                } else {
                    index = 0;
                    barDataSet.setColors(MATERIAL_COLORS[index++]);
                }
                lstBarDataSet.add(barDataSet);
            }

            BarData data = new BarData();
            data.getDataSets().addAll(lstBarDataSet);
            data.setValueFormatter(new LargeValueFormatter());

//            data.setValueTypeface(Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf"));

            barChart.setData(data);

        }

        // specify the width each bar should have
        barChart.getBarData().setBarWidth(barWidth);

        // restrict the x-axis range
        barChart.getXAxis().setAxisMinimum(start);

        // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters
        barChart.getXAxis().setAxisMaximum(start + barChart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
        barChart.groupBars(start, groupSpace, barSpace);
        barChart.animateXY(3000, 3000);
        barChart.invalidate();

    }

    public static void drawBarChart(BarChart barChart, List<ItemBean> lstItemBean, Context context) {
        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);

        barChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        barChart.setMaxVisibleValueCount(60);
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(false);

        MyMarkerView mv = new MyMarkerView(context, R.layout.custom_marker_view);
        mv.setChartView(barChart);
        barChart.setMarker(mv);


        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(true);
//        l.setTypeface(Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf"));
        l.setYOffset(0f);
        l.setXOffset(10f);
        l.setYEntrySpace(0f);
        l.setTextSize(8f);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);

//        IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = barChart.getAxisLeft();
//        leftAxis.setTypeface(mTfLight);
        leftAxis.setLabelCount(8, false);
//        leftAxis.setValueFormatter(custom);
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
//        rightAxis.setTypeface(mTfLight);
        rightAxis.setLabelCount(8, false);
//        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

//        Legend l = barChart.getLegend();
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
//        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
//        l.setDrawInside(false);
//        l.setForm(Legend.LegendForm.SQUARE);
//        l.setFormSize(9f);
//        l.setTextSize(11f);
//        l.setXEntrySpace(4f);

        float start = 1f;

        ArrayList<BarEntry> yVals1 = new ArrayList<>();

        int count = lstItemBean.size();
        //        int range = lstItemBean.get(0).getValue().intValue();
        for (int i = (int) start; i <= count; i++) {
            float val = lstItemBean.get(i-1).getValue();
            yVals1.add(new BarEntry(i, val));
        }

        BarDataSet set1;

        if (barChart.getData() != null &&
                barChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) barChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            barChart.getData().notifyDataChanged();
            barChart.notifyDataSetChanged();
        } else {

            List<BarDataSet> lstBarDataSet = new ArrayList<>();
            int index = 0;
            for(int i = 0; i< count; i++){
                List<BarEntry> lstBarEntry = new ArrayList<>();
                lstBarEntry.add(yVals1.get(i));

                BarDataSet barDataSet = new BarDataSet(lstBarEntry, lstItemBean.get(i).getName());
                if(index < MATERIAL_COLORS.length) {
                    barDataSet.setColors(MATERIAL_COLORS[index++]);
                } else {
                    index = 0;
                    barDataSet.setColors(MATERIAL_COLORS[index++]);
                }
                lstBarDataSet.add(barDataSet);
            }

            BarData data = new BarData();
            data.getDataSets().addAll(lstBarDataSet);
            data.setValueFormatter(new LargeValueFormatter());
            barChart.setData(data);


            float groupSpace = 0.08f;
            float barSpace = 0.03f; // x4 DataSet
            int elementGroup = 1;
//            float barWidth = 0.2f; // x4 DataSet
            float barWidth = barSpace * elementGroup + groupSpace;

            // specify the width each bar should have
            barChart.getBarData().setBarWidth(barWidth);

            // restrict the x-axis range
            barChart.getXAxis().setAxisMinimum(start);

            // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters
            barChart.getXAxis().setAxisMaximum(start + barChart.getBarData().getGroupWidth(groupSpace, barSpace) * elementGroup);
            barChart.groupBars(start, groupSpace, barSpace);
//
//            BarData data = new BarData();
//            data.getDataSets().addAll(lstBarDataSet);
//            data.setValueFormatter(new LargeValueFormatter());
//
////            data.setValueTypeface(Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf"));
//
//            barChart.setData(data);
//
//            set1 = new BarDataSet(yVals1, "Report mBCCS");
//            set1.setColors(MATERIAL_COLORS);
//
//            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
//            dataSets.add(set1);
//
//            BarData data = new BarData(dataSets);
//            data.setValueTextSize(10f);
////            data.setValueTypeface(mTfLight);
//            data.setBarWidth(0.9f);
//
//            barChart.setData(data);

            barChart.animateXY(3000, 3000);
            barChart.invalidate();
        }

    }
}
