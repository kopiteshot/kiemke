package com.viettel.bss.viettelpos.v4.stockinpect.control.model;

import org.simpleframework.xml.Root;

import java.io.Serializable;

import javax.xml.datatype.XMLGregorianCalendar;

@Root(name = "stockInspectRealDTO", strict = false)
public class StockInspectRealDTO extends BaseDTO implements Serializable {
    protected XMLGregorianCalendar createDate;
    protected String fromSerial;
    protected Long impType;
    protected String productOfferName;
    protected Long quantity;
    protected String reason;
    protected String serial2D;
    protected String singleSerial;
    protected Long stockInspectId;
    protected Long stockInspectRealId;
    protected String toSerial;

    public XMLGregorianCalendar getCreateDate() {
        return createDate;
    }

    public String getFromSerial() {
        return fromSerial;
    }

    public Long getImpType() {
        return impType;
    }

    public String getProductOfferName() {
        return productOfferName;
    }

    public Long getQuantity() {
        return quantity;
    }

    public String getReason() {
        return reason;
    }

    public String getSerial2D() {
        return serial2D;
    }

    public String getSingleSerial() {
        return singleSerial;
    }

    public Long getStockInspectId() {
        return stockInspectId;
    }

    public Long getStockInspectRealId() {
        return stockInspectRealId;
    }

    public String getToSerial() {
        return toSerial;
    }

    public void setCreateDate(XMLGregorianCalendar createDate) {
        this.createDate = createDate;
    }

    public void setFromSerial(String fromSerial) {
        this.fromSerial = fromSerial;
    }

    public void setImpType(Long impType) {
        this.impType = impType;
    }

    public void setProductOfferName(String productOfferName) {
        this.productOfferName = productOfferName;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setSerial2D(String serial2D) {
        this.serial2D = serial2D;
    }

    public void setSingleSerial(String singleSerial) {
        this.singleSerial = singleSerial;
    }

    public void setStockInspectId(Long stockInspectId) {
        this.stockInspectId = stockInspectId;
    }

    public void setStockInspectRealId(Long stockInspectRealId) {
        this.stockInspectRealId = stockInspectRealId;
    }

    public void setToSerial(String toSerial) {
        this.toSerial = toSerial;
    }
}
