package com.viettel.bss.viettelpos.v4.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.activity.FragmentEditCustomerBCCS;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.FragmentCommon;
import com.viettel.bss.viettelpos.v4.adapter.MProfilePagerAdapter;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.connecttionMobile.beans.ActionProfileBean;
import com.viettel.bss.viettelpos.v4.connecttionMobile.beans.RecordBean;
import com.viettel.bss.viettelpos.v4.utils.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * Created by Toancx on 6/12/2017.
 */
public class ModifyProfileFragment extends FragmentCommon {

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.vpPager)
    ViewPager viewPager;

    private ActionProfileBean actionProfileBean;
    private List<ArrayList<RecordBean>> lstRecordArray = new ArrayList<>();
    private MProfilePagerAdapter adapter;
    private Map<Integer, Fragment> mapFragment = new HashMap<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idLayout = R.layout.layout_menu_listview;
    }

    @Override
    protected void unit(View v) {
        Bundle bundle = getArguments();
        actionProfileBean = (ActionProfileBean) bundle.get("actionProfileBean");
        lstRecordArray = (List<ArrayList<RecordBean>>) bundle.getSerializable("lstRecordArray");

        if (Constant.PROFILE_STATUS.HS_GIA_MAO.equals(actionProfileBean.getTypeStatus())
                || Constant.PROFILE_STATUS.HS_SCAN_TIEPNHAN_CHUAKT.equals(actionProfileBean.getTypeStatus())) {
            tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.txt_suasaihs)));
            mapFragment.put(0, new UpdateProfileFragment());
            if(actionProfileBean.getCustId() != null && actionProfileBean.getCustId() > 0) {
                tabLayout.addTab(tabLayout.newTab().setText(R.string.editCustomer));
                mapFragment.put(1, new FragmentEditCustomerBCCS(
                        FragmentEditCustomerBCCS.NON_LOAD_DATA_WHILE_FINISH));
            }
        } else if (Constant.PROFILE_STATUS.HS_SCAN_SAI.equals(actionProfileBean.getTypeStatus())
                || Constant.PROFILE_STATUS.HS_CHUA_SCAN.equals(actionProfileBean.getTypeStatus())
                || Constant.PROFILE_STATUS.HS_SCAN_THIEU.equals(actionProfileBean.getTypeStatus())) {
            tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.txt_suasaihs)));
            mapFragment.put(0, new UpdateProfileFragment());
        } else if (Constant.PROFILE_STATUS.HS_SAI_THONG_TIN.equals(actionProfileBean.getTypeStatus())) {
            tabLayout.addTab(tabLayout.newTab().setText(R.string.editCustomer));
            mapFragment.put(0, new FragmentEditCustomerBCCS(
                    FragmentEditCustomerBCCS.LOAD_DATA_WHILE_FINISH));
        }

        adapter = new MProfilePagerAdapter(getFragmentManager(), lstRecordArray, actionProfileBean, mapFragment);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                // init data cho fragmentEditCustomerBCCS
                if (mapFragment.get(tab.getPosition()) instanceof FragmentEditCustomerBCCS) {
                    FragmentEditCustomerBCCS fragmentEditCustomerBCCS =
                            (FragmentEditCustomerBCCS) mapFragment.get(tab.getPosition());
                    fragmentEditCustomerBCCS.initDataUI();
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        // neu chi co 1 tab thi gone no di
        if (mapFragment.size() < 2) {
            tabLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void setPermission() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = adapter.getCurrentFragment(viewPager.getCurrentItem());
        fragment.onActivityResult(requestCode, resultCode, data);
    }
}
