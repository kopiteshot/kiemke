package com.viettel.bss.viettelpos.v4.customer.object;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by bavv on 1/10/18.
 */

@Root(name = "return", strict = false)
public class QRInfoBean {

    @Element(name = "errorCode", required = false)
    private String errorCode;

    @Element(name = "description", required = false)
    private String description;

    @Element(name = "success", required = false)
    private boolean success;

    @Element(name = "qrInfoBean", required = false)
    private QRCodeModel qrCodeModel;

    public QRCodeModel getQrCodeModel() {
        return qrCodeModel;
    }

    public void setQrCodeModel(QRCodeModel qrCodeModel) {
        this.qrCodeModel = qrCodeModel;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
