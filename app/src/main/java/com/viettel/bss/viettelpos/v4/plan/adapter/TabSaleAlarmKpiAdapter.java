package com.viettel.bss.viettelpos.v4.plan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.StringUtils;
import com.viettel.bss.viettelpos.v4.plan.model.AlarmKpiCurrent;
import com.viettel.bss.viettelpos.v4.plan.model.SaleResultCtv;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by BaVV on 9/1/2018.
 */

public class  TabSaleAlarmKpiAdapter extends RecyclerView.Adapter<TabSaleAlarmKpiAdapter.SaleAlarmKpiHolder> {

    private Context context;
    private List<AlarmKpiCurrent> alarmKpiCurrentList;
    private SaleWarningClickListener clickListener;

    public TabSaleAlarmKpiAdapter(Context context, List<AlarmKpiCurrent> alarmKpiCurrentList, SaleWarningClickListener listener) {
        this.context = context;
        this.alarmKpiCurrentList = alarmKpiCurrentList;
        this.clickListener = listener;
    }

    @Override
    public SaleAlarmKpiHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SaleAlarmKpiHolder(LayoutInflater.from(context).inflate(R.layout.adapter_item_tab_sale_alarm_kpi_view, parent, false));
    }

    @Override
    public void onBindViewHolder(SaleAlarmKpiHolder holder, int position) {
        final AlarmKpiCurrent alarmKpiCurrent = alarmKpiCurrentList.get(position);
        if (null == alarmKpiCurrent) return;

        holder.tvIndex.setText((position + 1) + ".");
        holder.tvKpiCode.setText(alarmKpiCurrent.getKpiCode());
        holder.tvKpiTargetDay.setText(alarmKpiCurrent.getKpiTargetDay() + "");
        holder.tvPerProgressDay.setText(alarmKpiCurrent.getPerProgressDay() + "");
        holder.tvStaffProcess.setText(alarmKpiCurrent.getStaffProcess());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != clickListener) clickListener.onClick(alarmKpiCurrent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return null == alarmKpiCurrentList ? 0 : alarmKpiCurrentList.size();
    }

    public class SaleAlarmKpiHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvIndex)
        TextView tvIndex;

        @BindView(R.id.tvKpiCode)
        TextView tvKpiCode;

        @BindView(R.id.tvKpiTargetDay)
        TextView tvKpiTargetDay;

        @BindView(R.id.tvPerProgressDay)
        TextView tvPerProgressDay;

        @BindView(R.id.tvStaffProcess)
        TextView tvStaffProcess;

        private SaleAlarmKpiHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public interface SaleWarningClickListener {
        void onClick(AlarmKpiCurrent alarmKpiCurrent);
    }
}
