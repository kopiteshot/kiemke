package com.viettel.bss.viettelpos.v4.stockinpect.control.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

@Root(name = "baseDTO", strict = false)
public class BaseDTO implements Serializable {
    @Element(name = "keySet", required = false)
    protected String keySet;

    public String getKeySet() {
        return keySet;
    }

    public void setKeySet(String keySet) {
        this.keySet = keySet;
    }
}
