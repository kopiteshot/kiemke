package com.viettel.bss.viettelpos.v4.connecttionService.asyn;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncTaskCommon;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

/**
 * Created by leekien on 2/9/2018.
 */

public class ApprovedManageRequestAsyn extends AsyncTaskCommon<String, String, Void> {
    public ApprovedManageRequestAsyn(Activity context, OnPostExecuteListener<Void> listener, View.OnClickListener moveLogInAct) {
        super(context, listener, moveLogInAct);
    }

    @Override
    protected Void doInBackground(String... params) {
        return confirmAppointmentRequest(params[0], params[1], params[2], params[3], params[4], params[5], params[6], params[7], params[8], params[9]);
    }

    private Void confirmAppointmentRequest(String actionType, String isdn, String taskId, String regNode, String reqDatetime, String reasonName, String resultNote, String resultDatetime, String id, String custOrderDetailId) {
        String original = "";
        ParseOuput out = null;
        String func = "confirmAppointmentRequest";
        try {

            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_" + func);
            StringBuilder rawData = new StringBuilder();
            rawData.append("<ws:" + func + ">");
            rawData.append("<input>");
            rawData.append("<token>" + Session.getToken() + "</token>");
            if (!CommonActivity.isNullOrEmpty(actionType))
                rawData.append("<actionType>" + actionType + "</actionType>");
            if (!CommonActivity.isNullOrEmpty(isdn))
                rawData.append("<isdn>" + isdn + "</isdn>");
            if (!CommonActivity.isNullOrEmpty(taskId))
                rawData.append("<taskId>" + taskId + "</taskId>");
            if (!CommonActivity.isNullOrEmpty(regNode))
                rawData.append("<regNode>" + regNode + "</regNode>");
            if (!CommonActivity.isNullOrEmpty(reqDatetime))
                rawData.append("<reqDatetime>" + reqDatetime + "</reqDatetime>");
            if (!CommonActivity.isNullOrEmpty(reasonName))
                rawData.append("<reasonName>" + reasonName + "</reasonName>");
            if (!CommonActivity.isNullOrEmpty(resultNote))
                rawData.append("<resultNote>" + resultNote + "</resultNote>");
            if (!CommonActivity.isNullOrEmpty(resultDatetime))
                rawData.append("<resultDatetime>" + resultDatetime + "</resultDatetime>");
            if (!CommonActivity.isNullOrEmpty(id))
                rawData.append("<id>" + id + "</id>");
            if (!CommonActivity.isNullOrEmpty(custOrderDetailId))
                rawData.append("<custOrderDetailId>" + custOrderDetailId + "</custOrderDetailId>");

            rawData.append("</input>");
            rawData.append("</ws:" + func + ">");
            Log.i("RowData", rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData
                    .toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope, Constant.BCCS_GW_URL,
                    mActivity, "mbccs_" + func);
            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i(" Original", response);

            // parser
            Serializer serializer = new Persister();
            out = serializer.read(ParseOuput.class, original);

            if (out == null) {
                out = new ParseOuput();
                errorCode = Constant.ERROR_CODE;
                out.setErrorCode(Constant.ERROR_CODE);
            } else {
                errorCode = out.getErrorCode();
                description = out.getDescription();
            }
        } catch (Exception e) {
            Log.e(Constant.TAG, func, e);
            description = e.getMessage();
            out = new ParseOuput();
            errorCode = Constant.ERROR_CODE;
            out.setErrorCode(Constant.ERROR_CODE);
            out.setDescription(description);
        }
        return null;
    }

}
