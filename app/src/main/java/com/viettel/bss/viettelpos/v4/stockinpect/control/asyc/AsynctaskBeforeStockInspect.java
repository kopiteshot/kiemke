package com.viettel.bss.viettelpos.v4.stockinpect.control.asyc;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.commons.BCCSGateway;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.CommonOutput;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.sale.asytask.AsyncTaskCommon;
import com.viettel.bss.viettelpos.v4.stockinpect.control.model.StockInspectCheckDTO;
import com.viettel.bss.viettelpos.v4.stockinpect.control.model.StockInspectDTO;
import com.viettel.bss.viettelpos.v4.work.object.ParseOuput;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.List;

public class AsynctaskBeforeStockInspect extends AsyncTaskCommon<Void, Void, ParseOuput> {
    StockInspectDTO checkStockInspectDTO;
    List<StockInspectCheckDTO> listProductCheck;
    String typeInspec;
    boolean confirmStockInspectApprover;

    public AsynctaskBeforeStockInspect(Activity context, OnPostExecuteListener<ParseOuput> listener, View.OnClickListener moveLogInAct, StockInspectDTO checkStockInspectDTO, List<StockInspectCheckDTO> listProductCheck, String typeInspec, boolean confirmStockInspectApprover) {
        super(context, listener, moveLogInAct);
        this.checkStockInspectDTO = checkStockInspectDTO;
        this.listProductCheck = listProductCheck;
        this.typeInspec = typeInspec;
        this.confirmStockInspectApprover = confirmStockInspectApprover;
    }

    @Override
    protected ParseOuput doInBackground(Void... voids) {
        String original = "";
        ParseOuput out = null;
        String func = "beforeStockInspectFormBCCS";
        try {
            BCCSGateway input = new BCCSGateway();
            input.addValidateGateway("username", Constant.BCCSGW_USER);
            input.addValidateGateway("password", Constant.BCCSGW_PASS);
            input.addValidateGateway("wscode", "mbccs_" + func);
            StringBuilder rawData = new StringBuilder();
            rawData.append("<ws:" + func + ">");
            rawData.append("<input>");
            rawData.append("<token>" + Session.getToken() + "</token>");

            rawData.append("<checkStockInspectDTO>");
            if (!CommonActivity.isNullOrEmpty(checkStockInspectDTO.getInspectStatus()))
                rawData.append("<inspectStatus>").append(checkStockInspectDTO.getInspectStatus()).append("</inspectStatus>");
            if (!CommonActivity.isNullOrEmpty(checkStockInspectDTO.getOwnerType()))
                rawData.append("<ownerType>").append(checkStockInspectDTO.getOwnerType()).append("</ownerType>");
            if (!CommonActivity.isNullOrEmpty(checkStockInspectDTO.getOwnerId()))
                rawData.append("<ownerId>").append(checkStockInspectDTO.getOwnerType()).append("</ownerId>");
            if (!CommonActivity.isNullOrEmpty(checkStockInspectDTO.getStateId()))
                rawData.append("<stateId>").append(checkStockInspectDTO.getStateId()).append("</stateId>");
            if (!CommonActivity.isNullOrEmpty(checkStockInspectDTO.getProdOfferTypeId()))
                rawData.append("<prodOfferTypeId>").append(checkStockInspectDTO.getProdOfferTypeId()).append("</prodOfferTypeId>");
            if (!CommonActivity.isNullOrEmpty(checkStockInspectDTO.getProdOfferId()))
                rawData.append("<prodOfferId>").append(checkStockInspectDTO.getProdOfferId()).append("</prodOfferId>");
            rawData.append("</checkStockInspectDTO>");

            rawData.append("<listProductCheck>");

            rawData.append("</listProductCheck>");


            rawData.append("<typeInspec>").append(typeInspec).append("</typeInspec>");
            rawData.append("<confirmStockInspectApprover>").append(confirmStockInspectApprover).append("</confirmStockInspectApprover>");


            rawData.append("</input>");
            rawData.append("</ws:" + func + ">");
            Log.i("RowData", rawData.toString());
            String envelope = input.buildInputGatewayWithRawData(rawData.toString());
            Log.d("Send evelop", envelope);
            Log.i("LOG", Constant.BCCS_GW_URL);
            String response = input.sendRequest(envelope, Constant.BCCS_GW_URL,
                    mActivity, "mbccs_" + func);
            Log.i("Responseeeeeeeeee", response);
            CommonOutput output = input.parseGWResponse(response);
            original = output.getOriginal();
            Log.i(" Original", response);

            // parser
            Serializer serializer = new Persister();
            out = serializer.read(ParseOuput.class, original);
        } catch (Exception e) {
            Log.e(Constant.TAG, e.getMessage(), e);
            description = e.getMessage();
            errorCode = Constant.ERROR_CODE;
        }
        if (CommonActivity.isNullOrEmpty(out)) {
            description = mActivity.getString(R.string.no_return_from_system);
            errorCode = Constant.ERROR_CODE;
        } else {
            description = out.getDescription();
            errorCode = out.getErrorCode();
        }
        return out;
    }
}
