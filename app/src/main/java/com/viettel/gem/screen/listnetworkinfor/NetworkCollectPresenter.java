package com.viettel.gem.screen.listnetworkinfor;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.activity.slidingmenu.MainActivity;
import com.viettel.bss.viettelpos.v4.bo.ApParamBO;
import com.viettel.bss.viettelpos.v4.business.ApParamBusiness;
import com.viettel.bss.viettelpos.v4.channel.activity.FragmentCusCareByDay;
import com.viettel.bss.viettelpos.v4.commons.CommonActivity;
import com.viettel.bss.viettelpos.v4.commons.Constant;
import com.viettel.bss.viettelpos.v4.commons.DateTimeUtils;
import com.viettel.bss.viettelpos.v4.commons.OnPostExecuteListener;
import com.viettel.bss.viettelpos.v4.commons.Session;
import com.viettel.bss.viettelpos.v4.dal.ApParamDAL;
import com.viettel.bss.viettelpos.v4.dialog.LoginDialog;
import com.viettel.bss.viettelpos.v4.object.ApParam;
import com.viettel.gem.asynctask.SearchCustomerAsyncTask;
import com.viettel.gem.base.viper.Presenter;
import com.viettel.gem.base.viper.interfaces.ContainerView;
import com.viettel.gem.model.SubscriberDTO;
import com.viettel.gem.model.SubscriberModel;
import com.viettel.gem.screen.collectinfo.CollectCustomerInfoPresenter;
import com.viettel.gem.screen.collectinfo.CollectInfoActivity;
import com.viettel.gem.screen.event.CollectEvent;
import com.viettel.gem.screen.listinforcustomer.ListInforCustomerPresenter;
import com.viettel.gem.screen.searchcustomer.ConfirmDialog;
import com.viettel.gem.utils.FileUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by root on 21/11/2017.
 */

public class NetworkCollectPresenter extends Presenter<NetworkCollectContract.View, NetworkCollectContract.Interactor>
        implements NetworkCollectContract.Presenter, NetworkCollectAdapter.NetworkCollectClickListener {
    private List<ApParamBO> mApParamBOList = new ArrayList<>();
    private NetworkCollectAdapter adapter;
    private boolean isChannelCare = false;
    private int currentPosition = 0;
    public NetworkCollectPresenter(ContainerView containerView) {
        super(containerView);
    }

    private List<SubscriberDTO> subscriberDTOList = new ArrayList<>();

    protected final View.OnClickListener moveLogInAct = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LoginDialog dialog = new LoginDialog(getViewContext(), "");
            dialog.show();
        }
    };

    @Override
    public void start() {
        initListCollect();
        setupTitle();
    }

    @Override
    public NetworkCollectContract.Interactor onCreateInteractor() {
        return new NetworkCollectInteractor(this);
    }

    @Override
    public NetworkCollectContract.View onCreateView(Bundle data) {
        return NetworkCollectFragment.getInstance();
    }

    @Override
    public void initListCollect() {
        mApParamBOList = ApParamBusiness.getLstApParamByName(getViewContext(), Constant.RULE_TTTTDB);
        adapter = new NetworkCollectAdapter(getViewContext(), mApParamBOList, this);
        mView.setAdapter(adapter);
    }

    public NetworkCollectPresenter setIsChannelCare(boolean isChannelCare){
        this.isChannelCare = isChannelCare;
        return this;
    }

    @Override
    public void collect(){
        ApParamBO apParamBO = mApParamBOList.get(currentPosition);
        apParamBO.setCollect(true);
        adapter.notifyItemChanged(currentPosition);
    }




    //    @Override
//    public void doSearch(String isdn, String idno) {
////        fakeSearch();//fake
//        getAllCustomer(isdn, idno);
//    }
//
//    @Override
//    public void showDialogNoData(final String isdn, final String idno) {
//        final ConfirmDialog confirmDialog = new ConfirmDialog(getViewContext(), new ConfirmDialog.ButtonClickDialog() {
//            @Override
//            public void onAddNew() {
//                //add new infor customer
//                new CollectCustomerInfoPresenter(mContainerView).setIsdn(isdn).setIdno(idno).setCreateNew(true).pushView();
//            }
//
//            @Override
//            public void onCancel() {
//            }
//        });
//        confirmDialog.show();
//    }

    @Override
    public void fakeSearch() {
        //FAKE
        String response = FileUtils.loadContentFromFile(getViewContext(), "xmls/search.xml");
        Serializer serializer = new Persister();
        SubscriberModel result = null;
        try {
            result = serializer.read(SubscriberModel.class, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (null != result
                && null != result.getLstSubscriberDTOExt()
                && !result.getLstSubscriberDTOExt().isEmpty()) {
            if (null == subscriberDTOList) {
                subscriberDTOList = new ArrayList<>();
            } else {
                subscriberDTOList.clear();
            }

            subscriberDTOList.addAll(result.getLstSubscriberDTOExt());

            new ListInforCustomerPresenter(mContainerView).setSubscriberDTOList(subscriberDTOList).pushView();
            Log.e("@@@@", result.toString());
        } else {
//            showDialogNoData("42342323525435", "2323123dsasad");
            Log.e("@@@@", "nulllllll");
        }
    }

    @Override
    public void collectInfor(String isdn, String idNo, String collectType, String title) {
//        new CollectCustomerInfoPresenter(mContainerView).setIsdn(isdn).setIdno(idNo).setCode(Constant.RULE_TTTTDB).setNetwork(netWork).setCreateNew(false).pushView();
        Bundle bundle = new Bundle();
        bundle.putString("mIsdn", isdn);
        bundle.putString("mIdNo", idNo);
        bundle.putString("mCollectType", collectType);
        bundle.putString("mTitle", title);

        Intent intent = new Intent(getViewContext(), CollectInfoActivity.class);
        intent.putExtras(bundle);

        getViewContext().startActivity(intent);
    }

    @Override
    public void onClick(View view, int position) {
        ApParamBO apParamBO = mApParamBOList.get(position);
        if(!CommonActivity.isNullOrEmpty(apParamBO)){
            String idNo = Session.userName;
            String isdn = apParamBO.getParType() + "_" + DateTimeUtils.convertDateTimeToString(new Date(), "yyyyMMdd");
            if(isChannelCare && !CommonActivity.isNullOrEmpty(FragmentCusCareByDay.staff)){
                idNo = FragmentCusCareByDay.staff.getStaffCode();
            }
//            isdn = idNo + "_" + isdn;

            setCurrentPosition(position);

            String collectType = Constant.RULE_TTTTDB + "_" + apParamBO.getParType();
            collectInfor(isdn, idNo, collectType, apParamBO.getParValue());
        }
    }

//    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
//    public void onMessageCollectEvent(CollectEvent event){
//        EventBus.getDefault().removeStickyEvent(event);
//        collect();
//    }
//
//    @Override
//    public void registerEventBus() {
//        super.registerEventBus();
//    }
//
//    @Override
//    public void unregisterEventBus() {
//        super.unregisterEventBus();
//    }

    private void setupTitle() {
        MainActivity.getInstance().setTitleActionBar(R.string.txt_collect_sale_point);
        MainActivity.getInstance().disableMenu();
    }

    public int getCurrentPosition(){
        return currentPosition;
    }

    private void setCurrentPosition(int position){
        this.currentPosition = position;
    }

    //    private void getAllCustomer(final String isdn, final String idno) {
//        new SearchCustomerAsyncTask(getViewContext(), new OnPostExecuteListener<SubscriberModel>() {
//            @Override
//            public void onPostExecute(SubscriberModel result, String errorCode, String description) {
//                if ("0".equals(errorCode)) {
//                    if (null != result
//                            && null != result.getLstSubscriberDTOExt()
//                            && !result.getLstSubscriberDTOExt().isEmpty()) {
//                        if (null == subscriberDTOList) {
//                            subscriberDTOList = new ArrayList<>();
//                        } else {
//                            subscriberDTOList.clear();
//                        }
//
//                        subscriberDTOList.addAll(result.getLstSubscriberDTOExt());
//
//                        new ListInforCustomerPresenter(mContainerView).setSubscriberDTOList(subscriberDTOList).pushView();
//                        Log.e("@@@@", result.toString());
//                    } else {
//                        showDialogNoData(isdn, idno);
//                    }
//                } else {
//                    showDialogNoData(isdn, idno);
//                }
//            }
//        }, moveLogInAct).execute(isdn, idno);
//    }
}
