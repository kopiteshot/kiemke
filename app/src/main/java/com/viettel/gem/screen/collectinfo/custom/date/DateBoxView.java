package com.viettel.gem.screen.collectinfo.custom.date;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.beans.ProductSpecCharDTO;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.gem.base.view.CustomView;
import com.viettel.gem.screen.collectinfo.CollectCustomerInfoPresenter;

import butterknife.BindView;


/**
 * Created by BaVV on 5/31/16.
 */
public class DateBoxView
        extends CustomView
        implements DateItemView.Callback {

    public final static String TELEVISION_EXPIRED_DATE = "TELEVISION_EXPIRED_DATE";
    public final static String INTERNET_EXPIRED_DATE = "INTERNET_EXPIRED_DATE";
//    public final static String CUST_EXP_CDT = "CUST_EXP_CDT";

    @BindView(R.id.boxDate)
    LinearLayout boxDate;

    @BindView(R.id.tvName)
    TextView tvName;

    private DateItemView mSelectedView;

    ProductSpecCharDTO productSpecCharDTO;

    public DateBoxView(Context context) {
        super(context);
    }

    public DateBoxView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DateBoxView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.cs_survey_date_box_view;
    }

    @Override
    public boolean validateView() {

        if(null != productSpecCharDTO){
            if(TELEVISION_EXPIRED_DATE.equals(productSpecCharDTO.getCode())
                    && CollectCustomerInfoPresenter.selectedOtherServiceTelevision == true)
                return true;

            if(INTERNET_EXPIRED_DATE.equals(productSpecCharDTO.getCode())
                    && CollectCustomerInfoPresenter.selectedOtherServiceInternet == true)
                return true;
        }
        return mSelectedView.validateView();
    }

    public void build(ProductSpecCharDTO productSpecCharDTO) {
        this.productSpecCharDTO = productSpecCharDTO;
        if (null == this.productSpecCharDTO) return;

        tvName.setText(this.productSpecCharDTO.getName());

        boxDate.removeAllViews();

        mSelectedView = new DateItemView(getContext());
        mSelectedView.build(productSpecCharDTO.getCode(), productSpecCharDTO.getValueType(), productSpecCharDTO.getValueData(), this);
        boxDate.addView(mSelectedView);
    }

    @Override
    public void onDateChanged(String text) {
        if (null != productSpecCharDTO) productSpecCharDTO.setValueData(text);
    }
}
