package com.viettel.gem.screen.collectinfo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.viettel.gem.base.ContainerActivity;
import com.viettel.gem.base.viper.ViewFragment;

public class CollectInfoActivity extends ContainerActivity {
    @Override
    public ViewFragment onCreateFirstFragment() {
        Bundle bundle = getData();
        String mIdNo = bundle.getString("mIdNo", "");
        String mIsdn = bundle.getString("mIsdn", "");
        String mCollectType = bundle.getString("mCollectType", "");
        String mTitle = bundle.getString("mTitle", "");

        return (ViewFragment) new CollectCustomerInfoPresenter(this)
                .setIdno(mIdNo)
                .setIsdn(mIsdn)
                .setTitle(mTitle)
                .setCollectType(mCollectType).getFragment();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FragmentManager manager = getSupportFragmentManager();
        onHandleFragmentResult(manager, requestCode, resultCode, data);
    }
}
