package com.viettel.gem.screen.collectinfo.custom.edittextplus;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viettel.bccs2.viettelpos.v2.connecttionMobile.beans.ProductSpecCharDTO;
import com.viettel.bss.viettelpos.v4.R;
import com.viettel.bss.viettelpos.v4.work.object.ProductSpecCharValueDTOList;
import com.viettel.gem.base.view.CustomView;
import com.viettel.gem.screen.collectinfo.custom.textview.TextItemView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * Created by Toancx on 3/23/2018.
 */

public class EditTextPlusBoxView extends CustomView
        implements EditTextPlusItemtView.Callback {
    @BindView(R.id.boxInput)
    LinearLayout boxInput;

    @BindView(R.id.tvName)
    TextView tvName;

    boolean required = true; //true bat buoc tat ca, false khong bat buoc tat ca

    String MAPPING = "MAPPING_";

    List<EditTextPlusItemtView> editTextPlusItemtViewList = new ArrayList<>();

    Map<String, List<ProductSpecCharValueDTOList>> mapProductSpecCharUseDTO;

    ProductSpecCharDTO productSpecCharDTO;

    public EditTextPlusBoxView(Context context) {
        super(context);
    }

    public EditTextPlusBoxView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditTextPlusBoxView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.cs_survey_edit_text_plus_box_view;
    }

    @Override
    public boolean validateView() {
        boolean flag = true;
        if(isRequired()) {
            for (EditTextPlusItemtView editTextPlusItemtView : editTextPlusItemtViewList) {
                if (!editTextPlusItemtView.validateView()) {
                    if(flag) flag = editTextPlusItemtView.validateView();
                }
            }
        } else {
            flag = false;
            for (EditTextPlusItemtView editTextPlusItemtView : editTextPlusItemtViewList) {
                if (editTextPlusItemtView.validateView() && !editTextPlusItemtView.ignoredValidateView()) {
                    if(!flag) flag = editTextPlusItemtView.validateView();
                }
            }
        }
        return flag;
    }

    @Override
    public void removeError() {
        super.removeError();
        for (EditTextPlusItemtView editTextPlusItemtView : editTextPlusItemtViewList) {
            editTextPlusItemtView.removeError();
        }
    }

    public boolean isRequired(){
        return required;
    }

    public EditTextPlusBoxView setRequired(boolean required){
        this.required = required;
        return this;
    }

    public void build(ProductSpecCharDTO productSpecCharDTO,  Map<String, List<ProductSpecCharValueDTOList>> mapProductSpecCharUseDTO) {
        this.productSpecCharDTO = productSpecCharDTO;
        if (null == this.productSpecCharDTO) return;

        tvName.setText(this.productSpecCharDTO.getName());

        boxInput.removeAllViews();

        this.mapProductSpecCharUseDTO = mapProductSpecCharUseDTO;

        List<ProductSpecCharValueDTOList> productSpecCharValueDTOList = productSpecCharDTO.getProductSpecCharValueDTOList();

        if (null != productSpecCharValueDTOList && !productSpecCharValueDTOList.isEmpty()) {
            for (ProductSpecCharValueDTOList specCharValueDTOList : productSpecCharValueDTOList) {
                EditTextPlusItemtView itemView = new EditTextPlusItemtView(getContext());

                itemView.build(this, specCharValueDTOList, mapProductSpecCharUseDTO.get(specCharValueDTOList.getId().replace(MAPPING, "")), this);
                boxInput.addView(itemView);
                editTextPlusItemtViewList.add(itemView);
            }
        }
    }



}
